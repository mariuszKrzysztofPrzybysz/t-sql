USE [Northwind]
GO

--Defining a functions
ALTER FUNCTION fn_LongDate
(
	@FullDate AS DATETIME
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @result AS VARCHAR(MAX) = ''

	 SET @result +=DATENAME(DW,@FullDate) + ' '
	 SET @result +=DATENAME(D,@FullDate)
	 SET @result +=
		CASE
			WHEN DAY(@FullDate) IN (1,21,31)
				THEN 'st'
			WHEN DAY(@FullDate) IN (2,22)
				THEN 'nd'
			WHEN DAY(@FullDate) IN (3,23)
				THEN 'rd'
			ELSE
				'th'
		END + ' '
	 SET @result +=DATENAME(M,@FullDate) + ' '
	 SET @result +=DATENAME(YY,@FullDate)

	 RETURN @result
END
GO


--Executing '[dbo].[functionName]'
PRINT [dbo].[fn_LongDate]('1985-04-28 12:12:12 ')
GO  
