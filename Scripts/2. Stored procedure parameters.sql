USE [Northwind]
GO

----CREATE
--ALTER PROCEDURE sp_GetCustomerById
--(
--	@Id AS NCHAR(5)
--)
--AS
--BEGIN
--	SELECT
--		*
--	FROM
--		[Customers] AS [c]
--	WHERE
--		[c].[CustomerID] = @Id
--END
--GO

----EXECUTE
--EXECUTE sp_GetCustomerById 'ALFKI'

--CREATING OPTIONAL PARAMETERS
ALTER PROCEDURE sp_GetCustomersByCompanyNameAndContractName
(
	@CompanyName AS NVARCHAR(40) = NULL
	,@ContractName AS NVARCHAR(30) = NULL 
)
AS
BEGIN
	SELECT
		*
	FROM
		[Customers] AS [c]
	WHERE
		(@CompanyName IS NULL OR [c].CompanyName LIKE '%' + @CompanyName + '%')
		AND (@ContractName IS NULL OR [c].[ContactName] LIKE '%' + @ContractName + '%')
END
GO

EXECUTE sp_GetCustomersByCompanyNameAndContractName @ContractName='Ana'

