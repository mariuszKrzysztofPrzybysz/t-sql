SELECT
	id AS 'Id'
	,long AS 'Longitude'
	,lat AS 'Latitude'
	,address as 'Address'
FROM
	OPENROWSET(BULK 'D:\geoJSON.json', SINGLE_CLOB) as jsonData
CROSS APPLY OPENJSON(jsonData.BulkColumn, '$.features')
	WITH
	(
		id int 'strict $.id'
		,long float '$.geometry.coordinates[0]'
		,lat float '$.geometry.coordinates[1]'
		,address nvarchar(100) '$.properties.ADDRESS'
	)
