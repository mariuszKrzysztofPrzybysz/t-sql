USE [Northwind]
GO

ALTER FUNCTION fn_FirstName
(
	@FullName AS VARCHAR(MAX)
)
RETURNS VARCHAR(MAX)
AS
BEGIN
	DECLARE @SpacePosition AS INTEGER
	DECLARE @result AS VARCHAR(MAX)


	SET @SpacePosition = CHARINDEX(' ', @FullName)

	IF @SpacePosition = 0
		SET @result = @FullName
	ELSE
		SET @result = LEFT(@FullName,@SpacePosition - 1)

	RETURN @result
END
GO

PRINT [dbo].[fn_FirstName]('Mariusz Przybysz')
GO