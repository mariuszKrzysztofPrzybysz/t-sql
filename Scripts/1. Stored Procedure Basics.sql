USE [Northwind]
GO

----CREATE

--CREATE PROCEDURE sp_CategoriesList
--AS
--BEGIN
--	SELECT
--		*
--	FROM
--		[Categories] AS [c]
--	ORDER BY
--		[c].[CategoryName]
--END

----EXECUTE

--EXECUTE sp_CategoriesList

----MODIFY

--ALTER PROCEDURE sp_CategoriesList
--AS
--BEGIN
--	SELECT
--		*
--	FROM
--		[Categories] AS [c]
--	ORDER BY
--		[c].[CategoryName]
--END

----DELETE
--DROP PROCEDURE sp_CategoriesList