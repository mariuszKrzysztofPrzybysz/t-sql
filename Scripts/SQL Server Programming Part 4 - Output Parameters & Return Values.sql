USE [Northwind]
GO

--DEFINING OUTPUT PARAMETERS
ALTER PROCEDURE sp_GetRegionForTeritory
(
	@TerritoryID AS NVARCHAR(20)
	,@RegionID AS INT OUTPUT
	,@RegionDescription AS NCHAR(50) OUTPUT
)
AS
BEGIN
	SELECT
		@RegionID = [r].RegionID, @RegionDescription = [r].RegionDescription
	FROM
		[dbo].[Territories] AS [t]
		INNER JOIN [dbo].[Region] AS [r]
			ON [t].RegionID = [r].[RegionID]
	WHERE
		[t].[TerritoryID] = @TerritoryID
END
GO 

DECLARE @Id AS INT
DECLARE @Description AS NCHAR(50)

EXECUTE sp_GetRegionForTeritory
	@TerritoryID = '01581'
	,@RegionID = @Id OUTPUT
	,@RegionDescription = @Description OUTPUT

PRINT 'Region id: ' + CONVERT(NVARCHAR(MAX), @Id) + ', description: ' + @Description 
GO

--ADDING A RETURN VALUE TO A PROCEDURE
 ALTER PROCEDURE sp_GetRegionForTeritory
(
	@TerritoryID AS NVARCHAR(20)
	,@RegionID AS INT OUTPUT
	,@RegionDescription AS NCHAR(50) OUTPUT
)
AS
BEGIN
	SELECT
		@RegionID = [r].RegionID, @RegionDescription = [r].RegionDescription
	FROM
		[dbo].[Territories] AS [t]
		INNER JOIN [dbo].[Region] AS [r]
			ON [t].RegionID = [r].[RegionID]
	WHERE
		[t].[TerritoryID] = @TerritoryID

	--RETURN ONLY A !!!NUMBER!!!
	RETURN  @@ROWCOUNT
END
GO

--READING THE RESULT OF A RETURN VALUE
DECLARE @Count AS INT

DECLARE @Id AS INT
DECLARE @Description AS NCHAR(50)

EXECUTE @Count = sp_GetRegionForTeritory
	@TerritoryID = '01581'
	,@RegionID = @Id OUTPUT
	,@RegionDescription = @Description OUTPUT

SELECT @Count AS [Number of regions]
GO