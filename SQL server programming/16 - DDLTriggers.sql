USE Northwind
GO

--CREATE TRIGGER trgNoNewTables
--ON DATABASE
--FOR CREATE_TABLE, ALTER_TABLE, DROP_TABLE
--AS
--BEGIN
		
--	PRINT 'No new tables please'
--	ROLLBACK
--END

--CREATE TABLE tblTest(ID INT)
--GO

--DROP TRIGGER trgNoNewTables ON DATABASE

--DISABLE TRIGGER trgNoNewTables ON DATABASE
--ENABLE TRIGGER trgNoNewTables ON DATABASE

--DISABLE TRIGGER ALL ON DATABASE
--GO
--ENABLE TRIGGER ALL ON DATABASE
--GO

--ALTER TRIGGER triggerAferCreateTable
--ON ALL SERVER
--FOR CREATE_TABLE
--AS
--BEGIN

--	PRINT 'New table created'

--END

--DISABLE TRIGGER ALL ON ALL SERVER
--GO

--CREATE TRIGGER triggerTheFirstTrigger
--ON DATABASE
--FOR CREATE_TABLE
--AS
--BEGIN
--	PRINT 'This is the first trigger'
--END
--GO

--CREATE TRIGGER triggerTheSecondTrigger
--ON DATABASE
--FOR CREATE_TABLE
--AS
--BEGIN
--	PRINT 'This is the second trigger'
--END
--GO

--EXECUTE sp_settriggerorder
--	@triggername = 'triggerTheFirstTrigger',
--	@order = 'first',--last, --none
--	@stmttype = 'CREATE_TABLE',
--	@namespace = 'DATABASE'

EXECUTE sp_settriggerorder
	@triggername = 'triggerTheFirstTrigger',
	@order = 'last',
	@stmttype = 'CREATE_TABLE',
	@namespace = 'DATABASE'