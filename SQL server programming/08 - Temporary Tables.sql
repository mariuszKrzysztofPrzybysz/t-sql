USE [Northwind]
GO

----Creating temporary tables #1
--SELECT
--	*
--	INTO #tempProducts
--FROM
--	Products p
--WHERE
--	p.ProductID < 10
--GO

--SELECT
--	*
--FROM
--	#tempProducts
--GO

----Creating temporary tables #2
--DROP TABLE IF EXISTS #tempProducts
--GO

--CREATE TABLE #tempProducts
--(
--	Id INT
--	,Name NVARCHAR(40)
--)

--INSERT INTO #tempProducts
--SELECT
--	p.ProductID
--	,p.ProductName
--FROM
--	Products p
--WHERE
--	p.ProductID < 10

--SELECT
--	tp.Name
--FROM
--	#tempProducts tp
--ORDER BY
--	tp.Name