USE [Northwind]
GO

--WITH ProductsBetween AS
--(
--	SELECT
--		*
--	FROM
--		[dbo].[Products] AS p
--	WHERE
--		p.ProductID BETWEEN 11 AND 20
--)
--SELECT
--	*
--FROM
--	ProductsBetween AS pb
--WHERE
--	pb.SupplierID IN (5, 6)

WITH CountSupplierIdTable
----Labelling columns in a CTE
--(
--	Company 
--	,[Companies per product]
--)
AS
(
	SELECT
		p.[SupplierID]
		,COUNT(p.[SupplierID]) AS [CountSupplierId]
	FROM
		[dbo].[Products] AS p
	GROUP BY
		p.[SupplierID]
)
SELECT
	s.[CompanyName] AS [Company]
	,t.[CountSupplierId]
FROM
	CountSupplierIdTable t
INNER JOIN [dbo].[Suppliers] s
	ON t.[SupplierID] = s.[SupplierID]