DROP TABLE IF EXISTS #tempTable
SELECT
	DISTINCT(c.[City])
		INTO #tempTable
FROM
	[Northwind].[dbo].[Customers] AS c
ORDER BY
	c.[City]

DECLARE @cityNames NVARCHAR(MAX) = ''
SELECT @cityNames += QUOTENAME(City) + ', ' FROM #tempTable
DROP TABLE #tempTable

SET @cityNames = LEFT(@cityNames, LEN(@cityNames)-1)

DECLARE @SQL NVARCHAR(MAX) = '
SELECT * FROM (
SELECT
	c.Country
	,COUNT(c.City) AS [Number of cities]
	,c.[City]
	
FROM
	Northwind..Customers AS c
GROUP BY
	c.[Country], c.[City]
	) AS BaseData
PIVOT (
	COUNT([Number of cities])
	FOR Country
	IN (' + @cityNames + 
	')
) AS PivotTable'

PRINT @SQL

EXECUTE sp_executesql @SQL