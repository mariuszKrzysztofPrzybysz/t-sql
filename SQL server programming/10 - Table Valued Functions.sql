--Defining an Inline Table-Valued Function
USE [AdventureWorks]
GO

ALTER FUNCTION fn_GetPersonBusinessEntityId
(
	@businessEntityId AS int
)
RETURNS TABLE
AS
RETURN (
	SELECT
		*
	FROM
		[Person].[Person] as p
	WHERE
		p.BusinessEntityID=@businessEntityId
)
GO

SELECT
	*
FROM
	[dbo].[fn_GetPersonBusinessEntityId](1)
GO

--Multi-Statement Table-Valued Functions (MSTVF)

CREATE FUNCTION fn_GetPeoplesIdAndVendorsIdBetweenModifiedDates (
	@startDate AS DATETIME
	,@endDate AS DATETIME
)
RETURNS @result TABLE (
	Id  INT
	,[Name] VARCHAR(6)
)
AS
BEGIN

	INSERT INTO @result
	SELECT
		person.BusinessEntityID
		,'Person'
	FROM
		[Person].[Person] AS person
	WHERE
		person.ModifiedDate>@startDate AND person.ModifiedDate<@endDate

		INSERT INTO @result
		SELECT
			vendor.BusinessEntityID
			,'Vendor'
		FROM
			[Purchasing].[Vendor] AS vendor
		WHERE
		vendor.ModifiedDate>@startDate AND vendor.ModifiedDate<@endDate

	RETURN
END