--What the Pivot Operator does

--SELECT
--	*
--FROM
--	(
--		SELECT
--			c.CompanyName
--			,c.Country
--			,c.City
--			,COUNT(o.OrderDate) AS [Number of orders]
--		FROM
--			Northwind..Orders AS o
--			INNER JOIN Northwind..Customers AS c
--				ON o.CustomerID=c.CustomerID
--		GROUP BY
--			c.CompanyName, c.Country, c.City
--	) AS BaseData
--PIVOT
--(
--	COUNT(BaseData.[Number of orders])
--	FOR CompanyName IN ([Alfreds Futterkiste], [Ana Trujillo Emparedados y helados], [Antonio Moreno Taquer�a], [Around the Horn])
--) AS PivotTable


--Selecting the Base Data
--Creating a temporary dataset

--SELECT
--	*
--FROM
--	(SELECT
--		t.TerritoryDescription
--		,r.RegionDescription
--	FROM
--		Northw ind..Territories AS t
--		INNER JOIN Northwind..Region AS r
--			ON t.RegionID = r.RegionID) AS BaseData

--Applying the Pivot operator

DECLARE @regionList VARCHAR(MAX) = ''
SELECT
	@regionList = @regionList + QUOTENAME(TRIM(r.RegionDescription), '[') + ', '
FROM
	Northwind..Region AS r

SET @regionList=LEFT(@regionList,LEN(@regionList)-1)

SELECT
	*
FROM
	(SELECT
		COUNT(t.TerritoryDescription) AS [Number of territories]
		,r.RegionDescription
	FROM
		Northwind..Territories AS t
		INNER JOIN Northwind..Region AS r
			ON t.RegionID = r.RegionID
	GROUP BY r.RegionDescription
	) AS BaseData
PIVOT
(
	SUM(BaseData.[Number of territories])
	FOR RegionDescription
	IN ([Eastern], [Northern], [Western], [Southern])
) AS PivotTable
--Quickly genereting a column list

--DECLARE @regionList VARCHAR(MAX) = ''
--SELECT
--	@regionList = @regionList + QUOTENAME(TRIM(r.RegionDescription), '[') + ', '
--FROM
--	Northwind..Region AS r

--PRINT LEN(@regionList)
--SET @regionList=LEFT(@regionList,LEN(@regionList)-1)
--PRINT @regionList
--Creating row groups
--Ordering the results