USE [Northwind]
GO

DECLARE @ID INT
DECLARE @Name NVARCHAR(40)

--Declaring a cursor
DECLARE ShipperCursor CURSOR SCROLL
	FOR
		SELECT
			ShipperID, CompanyName
		FROM
			[dbo].[Shippers] 
--Opening and closing cursors
OPEN ShipperCursor
	--Moving through a set of records
	FETCH NEXT FROM ShipperCursor
		INTO @Id, @Name
	WHILE @@FETCH_STATUS = 0
	BEGIN
		PRINT @Id
		IF @Id % 2 = 1
		BEGIN
			PRINT @Id*2
		END
		FETCH NEXT FROM ShipperCursor
			INTO @Id, @Name
	END
CLOSE ShipperCursor
DEALLOCATE ShipperCursor

----DECLARE CursorName CURSOR SCROLL
	----FOR SELECT ...
----FETCH FIRST - ustawia kursor na pierwszy rekord 
----FETCH LAST - ustawia kursor na ostatni rekord 
----FETCH PRIOR FROM - ustawia kursor na rekord jeden przed bie��cym 
----FETCH ABSOLUTE n - ustawia kursor na rekord o pozycji n w  zbiorze, n - liczba ca�kowita
----FETCH RELATIVE n - ustawi kursor na rekord o pozycji n od bie��cej, n - liczba ca�kowita 