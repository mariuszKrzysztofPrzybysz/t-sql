USE [Northwind]
GO

--Declaring a table variable
DECLARE @tempEmployees AS TABLE
(
	Id  INT
	,FullName NVARCHAR(30)
)

INSERT INTO @tempEmployees
SELECT
	e.EmployeeID
	,e.FirstName + ' ' + e.LastName
FROM
	[dbo].[Employees] e

SELECT
	*
FROM
	@tempEmployees te
ORDER BY
	te.FullName