--Finding Customers with no orders (Fissa and ...)
--SELECT
--	c.CustomerID
--	,o.OrderID
--FROM
--	Northwind..Customers AS c
--	LEFT OUTER JOIN Northwind..Orders AS o ON c.CustomerID = o.CustomerID
--WHERE
--	o.CustomerID IS NULL

--BEGIN TRY
--	BEGIN TRANSACTION DeleteFissa

--		DELETE FROM
--			Northwind..Customers
--		WHERE
--			Northwind..Customers.CustomerID = 'FISSA'

--		SELECT
--			*
--		FROM
--			Northwind..Customers
--		WHERE
--			Northwind..Customers.CustomerID = 'FISSA'

--		COMMIT TRANSACTION DeleteFissa
--END TRY
--BEGIN CATCH
--		ROLLBACK TRANSACTION DeleteFissa
--END CATCH

--SELECT
--	*
--FROM
--	Northwind..Customers
--WHERE
--	Northwind..Customers.CustomerID = 'FISSA'

PRINT @@TRANCOUNT

BEGIN TRANSACTION Transacion1

	PRINT @@TRANCOUNT

	SAVE TRANSACTION SavePoint

	PRINT @@TRANCOUNT

	BEGIN TRANSACTION Transacion2
	
		PRINT @@TRANCOUNT


	ROLLBACK TRANSACTION SavePoint

	PRINT @@TRANCOUNT

COMMIT TRANSACTION Transacion1