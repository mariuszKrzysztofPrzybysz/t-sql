USE Northwind
GO

--CREATE TRIGGER trCategoriesChanged
--ON dbo.Categories
--AFTER INSERT, UPDATE, DELETE
--AS
--BEGIN
--	PRINT 'Category has changed'
--END

DROP TRIGGER IF EXISTS triggerCategoryInserted

--ALTER TRIGGER triggerCategoryInserted
--ON dbo.Categories
--INSTEAD OF INSERT
--AS
--BEGIN
	
--	RAISERROR('No more Categories can be inserted',16,1)
		
--END
--GO

----!!! Cannot use text, ntext, or image columns in the 'inserted' and 'deleted' tables. !!!
ALTER TRIGGER trigger_AfterCategoryInserted
ON dbo.Categories
AFTER INSERT
AS
BEGIN

	SELECT CategoryID, CategoryName FROM inserted

END